﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioControl : MonoBehaviour
{
    public AudioSource audio;
    public AudioSource audio2;
    public GameObject child;

    public GameObject rotor;

    public GameObject plainer;

    public Transform[] plainerPos;

    Transform plainerTrans;
    // Start is called before the first frame update
    void Start()
    {
        audio = this.GetComponent<AudioSource>();
        audio2 = child.GetComponent<AudioSource>();
        plainerTrans = plainerPos[1];
        plainer.transform.position = plainerPos[1].position;

        plainer.transform.rotation = plainerPos[1].rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            audio.Play();
        }
        if (Input.GetMouseButtonUp(0))
        {
            audio.Stop();
            audio2.Play();

        }



        plainer.transform.position = Vector3.Lerp(plainer.transform.position, plainerTrans.position, Time.deltaTime * 7f);

        plainer.transform.rotation = Quaternion.Lerp(plainer.transform.rotation, plainerTrans.rotation, Time.deltaTime * 8f);

        if (Input.GetMouseButton(0))
        {
            plainerTrans = plainerPos[0];

            //plainer.transform.position = Vector3.Lerp(plainer.transform.position, plainerPos[0].position, Time.deltaTime * 5f);
        }
        else
        {
            //plainer.transform.position = Vector3.Lerp(plainer.transform.position, plainerPos[1].position, Time.deltaTime * 5f);
            plainerTrans = plainerPos[1];
        }

        rotor.transform.Rotate(0, 2 * Time.deltaTime * 50, 0);
    }
}
